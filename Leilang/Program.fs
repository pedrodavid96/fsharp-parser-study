﻿// Learn more about F# at http://fsharp.org

open System

type Token =
    | Number of int32
    | Name of string

let isDigit c = c > '0' && c < '9'
let digit c = (int c - int '0')

let tokenize (line : string) =
    line |> Seq.mapi (fun i character ->
            match character with
            | c when isDigit(c) ->
                let number = line |> Seq.takeWhile isDigit |> Seq.map digit |> Seq.reduce (fun acc elem -> acc * 10 + elem)
                (Number number, true, i)
            | _ ->
                (Name line, true, i)
        ) |> Seq.takeWhile (fun (token, found, i) -> not found)


let parseLine line = line

[<EntryPoint>]
let main argv =
    let arr = argv |> Array.map parseLine
    printfn "%A" arr
    argv |> Array.map tokenize |> Seq.iter (fun elem -> printfn "%A" elem)
    0 // return an integer exit code
